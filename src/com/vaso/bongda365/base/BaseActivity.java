package com.vaso.bongda365.base;

import android.app.Activity;
import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.inputmethod.InputMethodManager;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragmentActivity;
import com.chartboost.sdk.Chartboost;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.fragment.ClipsDetailsFragment;
import com.vaso.bongda365.fragment.HelpFragment;
import com.vaso.bongda365.fragment.HomeFragment;
import com.vaso.bongda365.fragment.NewsDetailsFragment;
import com.vaso.bongda365.fragment.SchedulerDetailsFragment;

public class BaseActivity extends SherlockFragmentActivity {
	
	public static final String TAG = BaseActivity.class.getSimpleName();
	
	public Context mContext;
	
	public ActionBar mActionBar;
	
	public ImageLoader imageLoader;
	
	public Chartboost cb;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mContext = this;
		mActionBar = getSupportActionBar();
		imageLoader = ImageLoader.getInstance();
		
		// config actionbar
		mActionBar.setBackgroundDrawable(new ColorDrawable(Color.parseColor("#ffda44")));
		
		// config chartboost
		cb = Chartboost.sharedChartboost();
		cb.onCreate(this, Constant.CHARTBOOST_APP_ID, Constant.CHARTBOOST_APP_SIGNATURE, null);
		cb.cacheMoreApps();
	}
	
	@Override
	protected void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		cb.onStart(this);
	}
	
	@Override
	protected void onStop() {
		// TODO Auto-generated method stub
		super.onStop();
		cb.onStop(this);
	}
	
	@Override
	protected void onDestroy() {
		// TODO Auto-generated method stub
		super.onDestroy();
		cb.onDestroy(this);
	}
	
	@Override
	public void onBackPressed() {
		
		if (cb.onBackPressed()) 
	        return; 
	    else 
	        super.onBackPressed();
	}
	
	/**
	 * Switch content with TAG
	 * @param tag
	 * @param addtoBackstack
	 * @param clearBackStack
	 */
	public void switchContent(String tag, boolean addtoBackstack, boolean clearBackStack) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, 
				android.R.anim.fade_in, android.R.anim.fade_out);
		
		if (clearBackStack) {
			getSupportFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}
		
		Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
		if (fragment == null) {
			if (tag.equalsIgnoreCase(HomeFragment.TAG)) {
				fragment = new HomeFragment();
				
			} else if (tag.equalsIgnoreCase(NewsDetailsFragment.TAG)) {
				fragment = new NewsDetailsFragment();
				
			} else if (tag.equalsIgnoreCase(ClipsDetailsFragment.TAG)) {
				fragment = new ClipsDetailsFragment();
				
			} else if (tag.equalsIgnoreCase(SchedulerDetailsFragment.TAG)) {
				fragment = new SchedulerDetailsFragment();
				
			} else if (tag.equalsIgnoreCase(HelpFragment.TAG)) {
				fragment = new HelpFragment();
				
			}
		}
		
		if (addtoBackstack) {
			transaction.replace(R.id.content_frame, fragment, tag).addToBackStack(null).commit();
		} else {
			transaction.replace(R.id.content_frame, fragment, tag).commit();
		}
	}
	
	/**
	 * Switch content with new fragment
	 * @param fragment
	 * @param tag
	 * @param addtoBackstack
	 * @param clearBackStack
	 */
	public void switchContent(Fragment fragment, String tag, boolean addtoBackstack, boolean clearBackStack) {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		transaction.setCustomAnimations(android.R.anim.fade_in, android.R.anim.fade_out, 
				android.R.anim.fade_in, android.R.anim.fade_out);
		
		if (clearBackStack) {
			getSupportFragmentManager().popBackStack(null,
					FragmentManager.POP_BACK_STACK_INCLUSIVE);
		}

		if (fragment != null) {
			if (addtoBackstack) {
				transaction.replace(R.id.content_frame, fragment, tag).addToBackStack(null).commit();
			} else {
				transaction.replace(R.id.content_frame, fragment, tag).commit();
			}
		}
	}
	
	/**
	 * hide soft keyboard
	 */
	public void hideSoftKeyboard() {
		InputMethodManager inputMethodManager = (InputMethodManager) this
				.getSystemService(Activity.INPUT_METHOD_SERVICE);
		if (this.getCurrentFocus() != null)
			inputMethodManager.hideSoftInputFromWindow(this.getCurrentFocus()
					.getWindowToken(), 0);
	}
}
