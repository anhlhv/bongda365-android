package com.vaso.bongda365.base;

import android.app.Application;
import android.content.Context;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.BitmapAjaxCallback;
import com.nostra13.universalimageloader.cache.disc.naming.Md5FileNameGenerator;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.assist.QueueProcessingType;
import com.nostra13.universalimageloader.utils.L;
import com.parse.Parse;
import com.parse.ParseInstallation;
import com.parse.PushService;
import com.vaso.bongda365.activity.MainActivity;

public class SoccerApplication extends Application {

	@Override
	public void onCreate() {
		super.onCreate();
		
		// init push notifications with Parse
		Parse.initialize(this, "ean6OBtBC3x5F6NhvAMyJ7qbwogNAp5Hvp8gEFnH", "ZRugMssy4Hj1o2clt0iKtuz0HxV3DGo1u7Vi8CeJ");
		PushService.setDefaultPushCallback(this, MainActivity.class);
		ParseInstallation.getCurrentInstallation().saveInBackground();
		
		// init networking
		initializeAq();
		
		// init imageloader
		initImageLoader(getApplicationContext());
	}
	
	private void initializeAq() {
		//set the max number of concurrent network connections, default is 4
		AjaxCallback.setNetworkLimit(4); 

		//set the max number of icons (image width <= 50) to be cached in memory, default is 20
		BitmapAjaxCallback.setIconCacheLimit(10);

		//set the max number of images (image width > 50) to be cached in memory, default is 20
		BitmapAjaxCallback.setCacheLimit(10);

		//set the max size of an image to be cached in memory, default is 1600 pixels (ie. 400x400)
		BitmapAjaxCallback.setPixelLimit(400 * 400);

		//set the max size of the memory cache, default is 1M pixels (4MB)
		BitmapAjaxCallback.setMaxPixelLimit(500000);  
	}

	public static void initImageLoader(Context context) {
		  L.disableLogging();
		  // This configuration tuning is custom. You can tune every option, you
		  // may tune some of them,
		  // or you can create default configuration by
		  // ImageLoaderConfiguration.createDefault(this);
		  // method.
		  
		  DisplayImageOptions option = new DisplayImageOptions.Builder()
		    .showImageOnLoading(android.R.color.white)
		    .showImageForEmptyUri(android.R.color.white)
		    .showImageOnFail(android.R.color.white)
		    .imageScaleType(ImageScaleType.IN_SAMPLE_INT)
		    .cacheInMemory(true).cacheOnDisc(true).build();
		  
		  ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(context)
		  	.threadPriority(Thread.NORM_PRIORITY)
		    .threadPoolSize(3)
		    .denyCacheImageMultipleSizesInMemory()
		    .discCacheFileNameGenerator(new Md5FileNameGenerator())
		    .tasksProcessingOrder(QueueProcessingType.LIFO)
		    .defaultDisplayImageOptions(option)
		    .writeDebugLogs() // Remove for release app
		    .build();
		  // Initialize ImageLoader with configuration.
		  ImageLoader.getInstance().init(config);
		 }

	@Override
	public void onLowMemory() {
		//clear all memory cached images when system is in low memory
		//note that you can configure the max image cache count, see CONFIGURATION
		BitmapAjaxCallback.clearCache();
		ImageLoader.getInstance().clearDiscCache();
		ImageLoader.getInstance().clearMemoryCache();
	}
}
