package com.vaso.bongda365.base;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

public class Utils {

	public static String getValueForKey(Context mContext, String key) 
	{
		SharedPreferences prefs = mContext.getSharedPreferences(Constant.PREFS_NAME, 0);
		return prefs.getString(key, "");
	} 
	
	
	public static void saveValueForKey(Context mContext, String key, String value) 
	{
		SharedPreferences prefs = mContext.getSharedPreferences(Constant.PREFS_NAME, 0);
		Editor editor = prefs.edit();
		editor.putString(key, value);
		editor.commit();
	}
	
	public static void saveIntForKey(Context mContext, String key, int value) 
	{
		SharedPreferences prefs = mContext.getSharedPreferences(Constant.PREFS_NAME, 0);
		Editor editor = prefs.edit();
		editor.putInt(key, value);
		editor.commit();
	}
	
	public static int getIntForKey(Context context, String key) 
	{
		SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_NAME, 0);
		return prefs.getInt(key, -1);
	}
	
	public static boolean getBooleanForKey(Context context, String key) 
	{
		SharedPreferences prefs = context.getSharedPreferences(Constant.PREFS_NAME, 0);
		return prefs.getBoolean(key, false);
	}
	
	
	public static void saveBooleanForKey(Context mContext, String key, boolean value) 
	{
		SharedPreferences prefs = mContext.getSharedPreferences(Constant.PREFS_NAME, 0);
		Editor editor = prefs.edit();
		editor.putBoolean(key, value);
		editor.commit();
	}
	
	@SuppressLint("SimpleDateFormat")
	public static String reformatDateTime(String date){
		DateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSZ");
		DateFormat outputSdf = new SimpleDateFormat("dd-MM-yyyy HH:mm");
		try {
			Date parseDate = sdf.parse(date);
			sdf.setTimeZone(TimeZone.getTimeZone("IST"));
			
			return outputSdf.format(parseDate);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		
		return "";
	}
}
