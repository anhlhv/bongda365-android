package com.vaso.bongda365.base;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.vaso.bongda365.model.MClips;
import com.vaso.bongda365.model.MMenu;
import com.vaso.bongda365.model.MNews;

public class DataParsingController {
	
	public static final String TAG = "DataParsingController";

	/**
	 * TODO parse left menu categories
	 * @param json
	 * @return
	 */
	public static List<MMenu> parseLeftMenus(JSONArray jsonArr) {
		List<MMenu> lstMenu = new ArrayList<MMenu>();
		
		try {
			for (int i = 0; i < jsonArr.length(); i++) {
				MMenu mMenu = MMenu.createMMenuCategory(jsonArr.getJSONObject(i));
				lstMenu.add(mMenu);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "error parse list menu categories");
		}
		return lstMenu;
	}
	
	/**
	 * TODO parse tab menus
	 * @param jsonArr
	 * @return
	 */
	public static List<MMenu> parseTabMenus(JSONArray jsonArr){
		List<MMenu> lstMenu = new ArrayList<MMenu>();
		
		try {
			for (int i = 0; i < jsonArr.length(); i++) {
				MMenu mMenu = MMenu.createMMenu(jsonArr.getJSONObject(i));
				lstMenu.add(mMenu);
			}
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "error parse list menu categories");
		}
		return lstMenu;
	}
	
	/**
	 * TODO parse list news data in Home screen
	 * @param jsonArr
	 * @return
	 */
	public static List<MNews> parseNews(JSONArray jsonArr) {
		List<MNews> lstNews = new ArrayList<MNews>();
		try {
			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);
				MNews mNews = MNews.createMNews(jsonObj);
				lstNews.add(mNews);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "error parse json list news");
		}
		return lstNews;
	}
	
	/**
	 * TODO parse list clips data in Home screen
	 * @param jsonArr
	 * @return
	 */
	public static List<MClips> parseClips(JSONArray jsonArr) {
		List<MClips> lstClips = new ArrayList<MClips>();
		try {
			for (int i = 0; i < jsonArr.length(); i++) {
				JSONObject jsonObj = jsonArr.getJSONObject(i);
				MClips mClips = MClips.createMClips(jsonObj);
				lstClips.add(mClips);
			}
		} catch (Exception e) {
			e.printStackTrace();
			Log.e(TAG, "error parse json list clips");
		}
		return lstClips;
	}
	
}
