package com.vaso.bongda365.base;

public class Constant {
	
	/** DEVELOPER INFORMATION */
	public static final String DEVELOPER_EMAIL			=		"eric.anhhlv@gmail.com";
	public static final String APP_PACKAGE_NAME			=		"com.vaso.bongda365.activity";
	
	/** YOUTUBE DEVELOPER KEY */
	public static final String DEVELOPER_KEY			=		"AI39si4ck6-TNSdoLDfcn_Wt1xR8BO8O1Tw8vlYFLy6e68ehcYNQyCrrDUH-DxnSCKFEOCJ1Wng3C_JqfWJ1641W4CeQ_Se3-g";
	
	/** GCM SERVICE */
	public static final String SENDER_ID				=		"384639319368";
	
	/** CHARTBOOST */
	public static final String CHARTBOOST_APP_ID		=		"53c6caa289b0bb538d300c25";
	public static final String CHARTBOOST_APP_SIGNATURE	=		"56b6daeb2ba15ec329aeeddab515cf4a5ca4efba";

	/** PREFS FILE */
	public static final String PREFS_NAME				= 		"Soccer365PrefsFile";
	public static final String KEY_SAVED_LEFT_MENU		=		"saved_left_menu";
	public static final String KEY_SAVED_NEWS_TAB		=		"saved_news_tab";
	public static final String KEY_SAVED_CLIPS_TAB		=		"saved_clips_tab";
	public static final String KEY_SAVED_SCHEDULE_TAB	=		"saved_schedule_tab";
	public static final String KEY_SAVED_ACTIVED_TAB	=		"saved_actived_tab";
	public static final String KEY_SAVED_HELP			=		"saved_help_description";
	
	// for push notification
	public static final String KEY_SAVED_REGID			=		"registration_id";
	public static final String KEY_SAVED_APP_VERSION	=		"app_version";
	
	/** PARAMS */
	public static final String PARAM_SORT				=		"sort";
	public static final String PARAM_PAGE				=		"page";
	
	/** API PATH */
	public static final String PATH_HOME_MENU			=		"home";
	public static final String PATH_HOME_MATCH			=		"home/match";
	public static final String PATH_HOME_NEWS			=		"home/article";
	public static final String PATH_HOME_CLIPS			=		"home/video";
	public static final String PATH_RELATED_CLIPS		=		"video";
	
	/** BUNDLE PARAMS */
	public static final String BUNDLE_YOUTUBE_ID		=		"youtube_id";
	public static final String BUNDLE_CLIP_ID			=		"clip_id";
	public static final String BUNDLE_CLIP_TITLE		=		"clip_title";
	
	/** LEFT MENUS ID */
	public static final String LEFT_MENU_HOME			=		"left_menu_home";
	public static final String LEFT_MENU_NEWS			=		"left_menu_news";
	public static final String LEFT_MENU_SCHEDULER		=		"left_menu_scheduler";
	public static final String LEFT_MENU_CLIPS			=		"left_menu_clips";
	public static final String LEFT_MENU_HELP			=		"left_menu_help";
	
	/** TAB ACTIVE ID */
//	public static final String TAB_MENU_YESTERDAY		=		"tab_menu_yesterday";
//	public static final String TAB_MENU_TODAY			=		"tab_menu_today";
//	public static final String TAB_MENU_TOMORROW		=		"tab_menu_tomorrow";
	
	/** GOOGLE ADMOB */
	public static final String AD_BOTTOM_ONLIST			=		"ca-app-pub-6289429843649526/4014657958";
	
}
