package com.vaso.bongda365.base;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.View;
import android.widget.LinearLayout;

import com.actionbarsherlock.app.ActionBar;
import com.actionbarsherlock.app.SherlockFragment;
import com.chartboost.sdk.Chartboost;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.mediation.admob.AdMobExtras;
import com.nostra13.universalimageloader.core.ImageLoader;

public class BaseFragment extends SherlockFragment {
	
	public Context mContext;
	public ActionBar mActionBar;
	public ImageLoader imageLoader;
	
	public AdView adView;
	public AdRequest adRequest;
	
	public Chartboost cb;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		mContext = getActivity();
		imageLoader = ImageLoader.getInstance();
		mActionBar = getSherlockActivity().getSupportActionBar();
		cb = Chartboost.sharedChartboost();
		
		adView = new AdView(mContext);
	    adView.setAdSize(AdSize.BANNER);
	    adView.setAdUnitId(Constant.AD_BOTTOM_ONLIST);
	    Bundle bundle = new Bundle();
	    bundle.putString("color_bg", "000000");
	    AdMobExtras extras = new AdMobExtras(bundle);
	    adRequest = new AdRequest.Builder()
    		.addTestDevice("76E9747620ACFC02BAF8BA2592F44A08")
    		.addNetworkExtras(extras).build();
	}
	
	public void hideLayoutWhenFailedToLoadAds(final LinearLayout llAds) {
		adView.setAdListener(new AdListener() {

			@Override
			public void onAdFailedToLoad(int errorCode) {
				llAds.setVisibility(View.GONE);
			}
			
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				llAds.setBackgroundColor(Color.BLACK);
			}
			
		});
	}
	
	@Override
	public void onResume() {
		super.onResume();
		if (adView != null) {
			adView.resume();
		}
	}
	
	public void switchContent(String tag, boolean addtoBackstack, boolean clearBackStack){
		BaseActivity activity = (BaseActivity) getActivity();
		if (activity != null) {
			activity.switchContent(tag, addtoBackstack, clearBackStack);
		}
	}
	
	public void switchContent(Fragment fragment, String tag, boolean addtoBackstack, boolean clearBackStack) {
		BaseActivity activity = (BaseActivity) getActivity();
		if (activity != null) {
			activity.switchContent(fragment, tag, addtoBackstack, clearBackStack);
		}
	}

}
