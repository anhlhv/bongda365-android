package com.vaso.bongda365.activity;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Configuration;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.widget.DrawerLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.actionbarsherlock.view.Menu;
import com.actionbarsherlock.view.MenuItem;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseActivity;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.fragment.ClipsDetailsFragment;
import com.vaso.bongda365.fragment.HelpFragment;
import com.vaso.bongda365.fragment.HomeFragment;
import com.vaso.bongda365.fragment.NewsDetailsFragment;
import com.vaso.bongda365.fragment.SchedulerDetailsFragment;
import com.vaso.bongda365.model.MMenu;
import com.vaso.bongda365.widget.SherlockActionBarDrawerToggle;

public class MainActivity extends BaseActivity implements OnItemClickListener{
	
	public static final String TAG = MainActivity.class.getSimpleName();
	
	// UI component
	private DrawerLayout mDrawerLayout;
	private SherlockActionBarDrawerToggle mDrawerToggle;
	private ListView mLeftMenu;
	
	// data
	List<MMenu> menus;
	MenuAdapter adapter;
	
	String currentTitle;
	String title;
	String drawerTitle;
	
	private FragmentManager.OnBackStackChangedListener mOnBackStackChangedListener = new FragmentManager.OnBackStackChangedListener() {
		@Override
		public void onBackStackChanged() {
			int backStackEntryCount = getSupportFragmentManager().getBackStackEntryCount();
		    mDrawerToggle.setDrawerIndicatorEnabled(backStackEntryCount == 0);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		// init data
		menus = new ArrayList<MMenu>();
		
		// init component
		mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
		mLeftMenu = (ListView) findViewById(R.id.left_menu);
		
		title = drawerTitle = getTitle().toString();
		mDrawerToggle = new SherlockActionBarDrawerToggle(this, mDrawerLayout,
                R.drawable.ic_drawer, R.string.drawer_open, R.string.drawer_close) {

            /** Called when a drawer has settled in a completely closed state. */
            public void onDrawerClosed(View view) {
                super.onDrawerClosed(view);
                mActionBar.setTitle(title);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            /** Called when a drawer has settled in a completely open state. */
            public void onDrawerOpened(View drawerView) {
                super.onDrawerOpened(drawerView);
                mActionBar.setTitle(drawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };

        // Set the drawer toggle as the DrawerListener
        mDrawerLayout.setDrawerListener(mDrawerToggle);
        getSupportFragmentManager().addOnBackStackChangedListener(mOnBackStackChangedListener);
        
        // actionbar config
 		mActionBar.setDisplayHomeAsUpEnabled(true);
 		mActionBar.setHomeButtonEnabled(true);
 		
 		// set data for left menu
		adapter = new MenuAdapter(mContext, menus);
		mLeftMenu.setAdapter(adapter);
		mLeftMenu.setOnItemClickListener(this);
		
		// load data for left menu
		getMenus();
		
 		// show default Home fragment
		switchContent(HomeFragment.TAG, false, true);
		
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		setTitle(title);
	}
	
	@Override
	public void setTitle(CharSequence title) {
		this.title = title.toString();
		getSupportActionBar().setTitle(title);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		// If the nav drawer is open, hide action items related to the content view
//        boolean drawerOpen = mDrawerLayout.isDrawerOpen(mLeftMenu);
//        menu.findItem(R.id.action_websearch).setVisible(!drawerOpen);
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (mDrawerToggle.isDrawerIndicatorEnabled()
				&& mDrawerToggle.onOptionsItemSelected(item)) {
			return true;
		} 
		else if (item.getItemId() == android.R.id.home
				&& getSupportFragmentManager().popBackStackImmediate()) {
			return true;
		} 
		
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	protected void onPostCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onPostCreate(savedInstanceState);
		mDrawerToggle.syncState();
	}
	
	@Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        mDrawerToggle.onConfigurationChanged(newConfig);
    }
	
	private void getMenus() {
		menus.add(new MMenu(getString(R.string.left_menu_home), Constant.LEFT_MENU_HOME, R.drawable.ic_menu_home));
		menus.add(new MMenu(getString(R.string.left_menu_news), Constant.LEFT_MENU_NEWS, R.drawable.ic_menu_news));
		menus.add(new MMenu(getString(R.string.left_menu_scheduler), Constant.LEFT_MENU_SCHEDULER, R.drawable.ic_menu_scheduler));
		menus.add(new MMenu(getString(R.string.left_menu_clips), Constant.LEFT_MENU_CLIPS, R.drawable.ic_menu_clip));
		menus.add(new MMenu(getString(R.string.left_menu_help), Constant.LEFT_MENU_HELP, R.drawable.ic_menu_info));
	}
	
	@Override
	public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
		MMenu mMenu = menus.get(position);
		if (mMenu.getSubMenus() == null ||mMenu.getSubMenus().isEmpty()) {
			selectItem(position);
		}
	}
	
	/** Swaps fragments in the main content view */
	private void selectItem(int position) {
		MMenu mMenu = menus.get(position);
		// Insert the fragment by replacing any existing fragment
//		Bundle bundle = new Bundle();
//		bundle.putString(Constant.BUNDLE_MENU_ID, mMenu.getMenuId());
		if (mMenu.getMenuId().equals(Constant.LEFT_MENU_NEWS)) {
			switchContent(NewsDetailsFragment.TAG, false, true);
			setTitle(mMenu.getTitle());
			
		} else if (mMenu.getMenuId().equals(Constant.LEFT_MENU_HOME)) {
			switchContent(HomeFragment.TAG, false, true);
			setTitle(getString(R.string.app_name));
			
		} else if (mMenu.getMenuId().equals(Constant.LEFT_MENU_CLIPS)) {
			switchContent(ClipsDetailsFragment.TAG, false, true);
			setTitle(mMenu.getTitle());
			
		} else if (mMenu.getMenuId().equals(Constant.LEFT_MENU_SCHEDULER)) {
			switchContent(SchedulerDetailsFragment.TAG, false, true);
			setTitle(mMenu.getTitle());
			
		} else if (mMenu.getMenuId().equals(Constant.LEFT_MENU_HELP)) {
			switchContent(HelpFragment.TAG, false, true);
			setTitle(mMenu.getTitle());
		}
//		else if (mMenu.getSubType().equals("system_settings")) {
//			Intent intent = new Intent(this, SettingsActivity.class);
//			startActivity(intent);
//			
//		} else if (mMenu.getSubType().equals(Constant.TYPE_STREAM) && !mMenu.getMenuId().equals("allstream")) {
//			Intent intent = new Intent(this, LiveStreamActivity3.class);
//			intent.putExtra(Constant.BUNDLE_MENU_ID, mMenu.getMenuId());
//			startActivity(intent);
//			
//		} else {
//			switchContent(ContentFragment.TAG, bundle, false, true);
//			setTitle(mMenu.getTitle());
//		}

		// Highlight the selected item, update the title, and close the drawer
		mLeftMenu.setItemChecked(position, true);
		mDrawerLayout.closeDrawer(mLeftMenu);
	}

	/******************************************************************
	 ************************** ADAPTER *******************************
	 ******************************************************************/
	public class MenuAdapter extends BaseAdapter{
		
		private LayoutInflater mInflater;
		private List<MMenu> menus;
		private MenuHolder holder;
		
		public MenuAdapter(Context mContext, List<MMenu> menus) {
			mInflater = LayoutInflater.from(mContext);
			this.menus = menus;
		}

		@Override
		public int getCount() {
			return menus.size();
		}

		@Override
		public Object getItem(int position) {
			return menus.get(position);
		}

		@Override
		public long getItemId(int position) {
			return position;
		}

		@SuppressLint("DefaultLocale")
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			MMenu menu = menus.get(position);
			
			if (convertView == null) {
				holder = new MenuHolder();
				convertView = mInflater.inflate(R.layout.row_left_menu, parent, false);
				holder.tvMenuTitle = (TextView) convertView.findViewById(R.id.tvMenuTitle);
				holder.ivIcon = (ImageView) convertView.findViewById(R.id.ivIcon);
				// style menu
				holder.tvMenuTitle.setTypeface(null, Typeface.NORMAL);
				holder.tvMenuTitle.setTextColor(Color.parseColor("#ffffff"));
				
				convertView.setTag(holder);
				
			} else {
				holder = (MenuHolder) convertView.getTag();
			}
			
			holder.tvMenuTitle.setText(menu.getTitle());
			holder.ivIcon.setImageResource(menu.getIcon());
			
			return convertView;
		}
		
	}
	
	public static class MenuHolder{
		TextView tvMenuTitle;
		ImageView ivIcon;
	}
	
}
