package com.vaso.bongda365.activity;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager.LayoutParams;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.actionbarsherlock.view.MenuItem;
import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdSize;
import com.google.android.gms.ads.AdView;
import com.google.android.gms.ads.mediation.admob.AdMobExtras;
import com.google.android.youtube.player.YouTubeInitializationResult;
import com.google.android.youtube.player.YouTubePlayer;
import com.google.android.youtube.player.YouTubePlayer.OnFullscreenListener;
import com.google.android.youtube.player.YouTubePlayer.OnInitializedListener;
import com.google.android.youtube.player.YouTubePlayer.Provider;
import com.google.android.youtube.player.YouTubePlayerSupportFragment;
import com.paging.listview.PagingListView;
import com.paging.listview.PagingListView.Pagingable;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseActivity;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.base.DataParsingController;
import com.vaso.bongda365.model.MClips;
import com.vaso.bongda365.networking.SoccerAPI;

public class ClipsContentActivity extends BaseActivity implements OnInitializedListener, OnFullscreenListener{

	public static final String TAG = ClipsContentActivity.class.getSimpleName();
	
	private static final int RECOVERY_DIALOG_REQUEST = 1;

	// View components
	private PullToRefreshLayout mPullToRefreshLayout;
	private YouTubePlayerSupportFragment playerFragment;
	private LinearLayout contentLayout;
	private PagingListView lvRelated;
	private LinearLayout llAds;
	
	// Data
	String clipId;
	String clipTitle;
	String youtubeId;
	List<MClips> clips;
	ClipsAdapter adapter;
	int currentPos = -1;
	YouTubePlayer player;
	AdView adView;
	
	// paging
	public int page;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_content_clips);
		
		clips = new ArrayList<MClips>();
		
		// config action bar
		mActionBar.setDisplayHomeAsUpEnabled(true);
		setTitle(getString(R.string.left_menu_clips));
		
		// get data parameter
		clipId = getIntent().getExtras().getString(Constant.BUNDLE_CLIP_ID);
		youtubeId = getIntent().getExtras().getString(Constant.BUNDLE_YOUTUBE_ID);
		clipTitle = getIntent().getExtras().getString(Constant.BUNDLE_CLIP_TITLE);
		
		if (!TextUtils.isEmpty(clipTitle)) {
			setTitle(clipTitle);
		}
		
		// connect to UI components
		llAds = (LinearLayout) findViewById(R.id.layout_ads);
		mPullToRefreshLayout = (PullToRefreshLayout) findViewById(R.id.ptr_layout);
		contentLayout = (LinearLayout) findViewById(R.id.content_layout);
		lvRelated = (PagingListView) findViewById(R.id.lv_related);
		playerFragment = (YouTubePlayerSupportFragment) getSupportFragmentManager().findFragmentById(R.id.youtube_player_fragment);
		
		// init ads
		adView = new AdView(mContext);
	    adView.setAdSize(AdSize.BANNER);
	    adView.setAdUnitId(Constant.AD_BOTTOM_ONLIST);
	    Bundle bundle = new Bundle();
	    bundle.putString("color_bg", "000000");
	    AdMobExtras extras = new AdMobExtras(bundle);
	    AdRequest adRequest = new AdRequest.Builder()
		.addTestDevice("76E9747620ACFC02BAF8BA2592F44A08")
		.addNetworkExtras(extras).build();
		if(adView.getParent() != null){
			((ViewGroup) adView.getParent()).removeView(adView);
	     }
		llAds.addView(adView);
		adView.loadAd(adRequest);
		hideLayoutWhenFailedToLoadAds(llAds);
		
		// setup pull to refresh
		ActionBarPullToRefresh.from(this)
			.theseChildrenArePullable(lvRelated)
			.listener(new OnRefreshListener() {
		
				@Override
				public void onRefreshStarted(View view) {
					page = 0;
					clips.clear();
					getRelatedData();
				}
			}).setup(mPullToRefreshLayout);
		
		// init data
		playerFragment.initialize(Constant.DEVELOPER_KEY, this);
		adapter = new ClipsAdapter(mContext, R.layout.row_clips2, clips);
		lvRelated.setAdapter(adapter);
		lvRelated.setHasMoreItems(true);
		
		// init action listener
		lvRelated.setPagingableListener(new Pagingable() {
			
			@Override
			public void onLoadMoreItems() {
				getRelatedData();
			}
		});
		
		lvRelated.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> adapterView, View view, int position,
					long id) {
				if (currentPos != position) {
					currentPos = position;
					player.loadVideo(clips.get(position).getYoutubeId());
					setTitle(clips.get(position).getTitle());
				} else {
					Toast.makeText(mContext, getString(R.string.warning_play_current_clip), Toast.LENGTH_SHORT).show();
				}
			}
		});
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			finish();
			
		}
		return super.onOptionsItemSelected(item);
	}
	
	@Override
	public void onInitializationFailure(Provider provider,
			YouTubeInitializationResult errorReason) {
		if (errorReason.isUserRecoverableError()) {
			errorReason.getErrorDialog(this, RECOVERY_DIALOG_REQUEST).show();
			
		} else {
			String errorMessage = String.format(getString(R.string.error_player), errorReason.toString());
			Toast.makeText(this, errorMessage, Toast.LENGTH_LONG).show();
		}
	}

	@Override
	public void onInitializationSuccess(Provider provider, YouTubePlayer player,
			boolean wasRestored) {
		player.addFullscreenControlFlag(YouTubePlayer.FULLSCREEN_FLAG_CUSTOM_LAYOUT);
	    player.setOnFullscreenListener(this);

	    this.player = player;
	    if (!wasRestored) {
//	      player.cueVideo(mItem.getYoutubeId());
	      player.loadVideo(youtubeId);
	    }
	    
	}

	@Override
	public void onFullscreen(boolean fullscreen) {
		ViewGroup.LayoutParams playerParams = playerFragment.getView().getLayoutParams();
		if (fullscreen) {
			contentLayout.setVisibility(View.GONE);
			llAds.setVisibility(View.GONE);
			playerParams.width = LayoutParams.MATCH_PARENT;
			playerParams.height = LayoutParams.MATCH_PARENT;
		} else {
			contentLayout.setVisibility(View.VISIBLE);
			llAds.setVisibility(View.VISIBLE);
			playerParams.width = LayoutParams.MATCH_PARENT;
			playerParams.height = 0;
		}
	}
	
	public class ClipsAdapter extends ArrayAdapter<MClips>{
		
		List<MClips> clips;
		ClipsHolder holder;

		public ClipsAdapter(Context context, int resource, List<MClips> data) {
			super(context, resource, data);
			// TODO Auto-generated constructor stub
			this.clips = data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			MClips mClip = clips.get(position);
			
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_clips2, parent, false);
				holder = new ClipsHolder();
				holder.ivThumb = (ImageView) convertView.findViewById(R.id.iv_thumb);
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
				holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
				holder.tvViewCount = (TextView) convertView.findViewById(R.id.tv_view_count);
				
				convertView.setTag(holder);
			} else {
				holder = (ClipsHolder) convertView.getTag();
			}
			
			// setup data
			imageLoader.displayImage(mClip.getImageUrl(), holder.ivThumb);
			holder.tvTitle.setText(mClip.getTitle());
			holder.tvDate.setText(mClip.getPublishedAt());
			holder.tvViewCount.setText(String.valueOf(mClip.getViewCount()));
			
			return convertView;
		}
		
	}
	
	public static class ClipsHolder{
		ImageView ivThumb;
		TextView tvTitle;
		TextView tvDate;
		TextView tvViewCount;
	}
	
	private void getRelatedData() {
		lvRelated.setEnabled(false);
		page++;
		SoccerAPI.getInstance(mContext).getRelatedClips(clipId, page, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray jsonArr, AjaxStatus status) {
				List<MClips> lstTmp = DataParsingController.parseClips(jsonArr);
				if (lstTmp.isEmpty()) {
					lvRelated.onFinishLoading(false, null);
				} else {
					clips.addAll(lstTmp);
					lvRelated.onFinishLoading(true, clips);
				}
				
				if (mPullToRefreshLayout.isRefreshing()) {
					mPullToRefreshLayout.setRefreshComplete();
				}
				
				adapter.notifyDataSetChanged();
				lvRelated.setEnabled(true);
			}
		});
	}
	
	public void hideLayoutWhenFailedToLoadAds(final LinearLayout llAds) {
		adView.setAdListener(new AdListener() {

			@Override
			public void onAdFailedToLoad(int errorCode) {
				llAds.setVisibility(View.GONE);
			}
			
			@Override
			public void onAdLoaded() {
				super.onAdLoaded();
				llAds.setBackgroundColor(Color.BLACK);
			}
			
		});
	}
}
