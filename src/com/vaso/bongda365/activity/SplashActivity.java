package com.vaso.bongda365.activity;

import com.crashlytics.android.Crashlytics;
import org.json.JSONObject;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseActivity;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.base.Utils;
import com.vaso.bongda365.networking.SoccerAPI;

public class SplashActivity extends BaseActivity {

	public static final String TAG = SplashActivity.class.getSimpleName();

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		Crashlytics.start(this);
		getSupportActionBar().hide();
		setContentView(R.layout.activity_splash);
		
		new PreloadDataTask(new LoadMenuListener() {

			@Override
			public void onLoadMenuSuccess() {
				// delay 500ms
				new Handler().postDelayed(new Runnable() {
					
					@Override
					public void run() {
						Intent intent = new Intent(SplashActivity.this, MainActivity.class);
						startActivity(intent);
						finish();
					}
				}, 500);
			}

			@Override
			public void onLoadMenuFailed() {
				Toast.makeText(mContext, "Network error", Toast.LENGTH_LONG).show();;
			}
		}).execute();
	}

	private class PreloadDataTask extends AsyncTask<Void, Void, Void> {

		private LoadMenuListener listener;

		public PreloadDataTask(LoadMenuListener listener) {
			this.listener = listener;
		}

		@Override
		protected Void doInBackground(Void... params) {
			SoccerAPI.getInstance(mContext).getLeftMenuData(
					new AjaxCallback<JSONObject>() {
						@Override
						public void callback(String url, JSONObject json,
								AjaxStatus status) {
							try {
								if (json != null) {
//									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_LEFT_MENU, 
//											json.getJSONArray("menu").toString());

									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_NEWS_TAB, 
											json.getJSONArray("news_tabs").toString());

									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_CLIPS_TAB, 
											json.getJSONArray("video_tabs").toString());
									
									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_SCHEDULE_TAB, 
											json.getJSONArray("schedule_tabs").toString());
									
									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_ACTIVED_TAB, 
											json.getJSONObject("actived_tabs").toString());
									
									Utils.saveValueForKey(mContext, Constant.KEY_SAVED_HELP, 
											json.getJSONObject("info").toString());

									Log.e(TAG, "save home data success.");
									if (listener != null) {
										listener.onLoadMenuSuccess();
									}
								} else {
									Log.e(TAG, "new menu is null");
									if (listener != null) {
										listener.onLoadMenuFailed();
									}
								}
							} catch (Exception e) {
								e.printStackTrace();
								Log.e(TAG, "get new menu error");
								if (listener != null) {
									listener.onLoadMenuFailed();
								}
							}
						}
					});

			return null;
		}
	}

	public interface LoadMenuListener {

		public void onLoadMenuSuccess();

		public void onLoadMenuFailed();
	}
}
