package com.vaso.bongda365.widget;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.drawable.BitmapDrawable;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.vaso.bongda365.activity.R;

public class WebViewPopup extends PopupWindow implements OnClickListener{
	
	public static final String TAG = WebViewPopup.class.getSimpleName();
	
	// UI components
	WebView wvContent;
	Button btnClose;
	View rootView;
	
	// data
	Context mContext;
	String dataUrl;
	
	@SuppressWarnings("deprecation")
	public WebViewPopup(Context context, String dataUrl, int width, int height,
			View view, View parentView) {
		super(view, width, height, true);
		
		this.rootView = view;
		this.mContext = context;
		this.dataUrl = dataUrl;
		
		this.setAnimationStyle(R.style.AnimationPopupFade);
		this.setOutsideTouchable(true);
		this.setBackgroundDrawable(new BitmapDrawable());
		this.showAtLocation(parentView, Gravity.CENTER, 0, 0);
		
		findViews();
	}

	@SuppressLint("SetJavaScriptEnabled")
	private void findViews() {
		wvContent = (WebView) rootView.findViewById(R.id.wv_content);
		btnClose = (Button) rootView.findViewById(R.id.btn_close);
		btnClose.setOnClickListener(this);
		
		// load data to webview
		wvContent.getSettings().setJavaScriptEnabled(true); // enable javascript
		wvContent.setWebViewClient(new WebViewClient() {
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
                Toast.makeText(mContext, description, Toast.LENGTH_SHORT).show();
            }
        });
		wvContent.loadUrl(dataUrl);
	}

	@Override
	public void onClick(View v) {
		if (v == btnClose) {
			this.dismiss();
		}
	}

}
