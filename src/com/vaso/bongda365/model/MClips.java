package com.vaso.bongda365.model;

import org.json.JSONObject;

import com.google.gson.annotations.SerializedName;

public class MClips extends MBase {

	@SerializedName("id")
	private String id;

	@SerializedName("title")
	private String title;

	@SerializedName("image_url")
	private String imageUrl;

	@SerializedName("published_at")
	private String publishedAt;

	@SerializedName("youtube_id")
	private String youtubeId;

	@SerializedName("views_count")
	private int viewCount;

	@SerializedName("subtype")
	private String subType;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getImageUrl() {
		return imageUrl;
	}

	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}

	public String getPublishedAt() {
		return publishedAt;
	}

	public void setPublishedAt(String publishedAt) {
		this.publishedAt = publishedAt;
	}

	public String getYoutubeId() {
		return youtubeId;
	}

	public void setYoutubeId(String youtubeId) {
		this.youtubeId = youtubeId;
	}

	public int getViewCount() {
		return viewCount;
	}

	public void setViewCount(int viewCount) {
		this.viewCount = viewCount;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}
	
	public static MClips createMClips(JSONObject json){
		return gson.fromJson(json.toString(), MClips.class);
	}
}
