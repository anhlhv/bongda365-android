package com.vaso.bongda365.model;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class MMenu extends MBase {

	@SerializedName("_id")
	private String id;
	
	@SerializedName("title")
	private String title;
	
	@SerializedName("menu_id")
	private String menuId;
	
	@SerializedName("subtype")
	private String subType;
	
	private List<MMenu> subMenus;
	
	private int icon;
	
	public MMenu() {
	}
	
	// Constructor using for left menu
	public MMenu(String title, String menuId, int icon){
		this.title = title;
		this.menuId = menuId;
		this.icon = icon;
	}
	
	// Constructor using for tabs menu
	public MMenu(String title, String menuId){
		this.title = title;
		this.menuId = menuId;
	}

	public int getIcon() {
		return icon;
	}

	public void setIcon(int icon) {
		this.icon = icon;
	}

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getMenuId() {
		return menuId;
	}

	public void setMenuId(String menuId) {
		this.menuId = menuId;
	}

	public String getSubType() {
		return subType;
	}

	public void setSubType(String subType) {
		this.subType = subType;
	}

	public List<MMenu> getSubMenus() {
		return subMenus;
	}

	public void setSubMenus(List<MMenu> subMenus) {
		this.subMenus = subMenus;
	}
	
	/**
	 * Create menu with sub menus
	 * @param json
	 * @return
	 */
	public static MMenu createMMenuCategory(JSONObject json){
		MMenu mMenuCategory = null;
		List<MMenu> subMenus = new ArrayList<MMenu>();
		try {
			mMenuCategory = new MMenu();
			mMenuCategory.setId(json.getString("_id"));
			mMenuCategory.setTitle(json.getString("title"));
			mMenuCategory.setMenuId(json.getString("menu_id"));
			JSONArray arrSubMenu = json.getJSONArray("menu");
			for (int i = 0; i < arrSubMenu.length(); i++) {
				MMenu mSubMenu = gson.fromJson(arrSubMenu.get(i).toString(), MMenu.class);
				subMenus.add(mSubMenu);
			}
			mMenuCategory.subMenus = subMenus;
			
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("MMenu", "error parse menu");
		}
		return mMenuCategory;
	}
	
	/**
	 * Create menu with json object
	 * @param json
	 * @return
	 */
	public static MMenu createMMenu(JSONObject json){
		return gson.fromJson(json.toString(), MMenu.class);
	}
}
