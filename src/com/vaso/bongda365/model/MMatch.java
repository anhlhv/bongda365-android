package com.vaso.bongda365.model;

import org.json.JSONObject;

import android.util.Log;

import com.google.gson.annotations.SerializedName;

public class MMatch extends MBase {

	@SerializedName("id")
	private String id;
	
	@SerializedName("league")
	private String league;
	
	@SerializedName("started")
	private boolean started;
	
	@SerializedName("current_time")
	private String currentTime;
	
	@SerializedName("group")
	private String group;
	
	@SerializedName("tv_listing")
	private String tvListing;
	
	@SerializedName("referee")
	private String referee;
	
	@SerializedName("stadium")
	private String stadium;
	
	@SerializedName("time")
	private String time;
	
	@SerializedName("date")
	private String date;
	
	private MTeam team1;
	private MTeam team2;
	
	public String getId() {
		return id;
	}



	public void setId(String id) {
		this.id = id;
	}



	public String getLeague() {
		return league;
	}



	public void setLeague(String league) {
		this.league = league;
	}



	public boolean isStarted() {
		return started;
	}



	public void setStarted(boolean started) {
		this.started = started;
	}



	public String getCurrentTime() {
		return currentTime;
	}



	public void setCurrentTime(String currentTime) {
		this.currentTime = currentTime;
	}

	public String getGroup() {
		return group;
	}



	public void setGroup(String group) {
		this.group = group;
	}



	public String getTime() {
		return time;
	}



	public void setTime(String time) {
		this.time = time;
	}



	public String getDate() {
		return date;
	}



	public void setDate(String date) {
		this.date = date;
	}



	public MTeam getTeam1() {
		return team1;
	}



	public void setTeam1(MTeam team1) {
		this.team1 = team1;
	}



	public MTeam getTeam2() {
		return team2;
	}



	public void setTeam2(MTeam team2) {
		this.team2 = team2;
	}

	/**
	 * Create instance MMatch from json object
	 * @param json
	 * @return
	 */
	public static MMatch createMMatch(JSONObject json){
		MMatch mMatch = gson.fromJson(json.toString(), MMatch.class);
		try {
			// if any one in two team data is null, return no match
			if (json.isNull("team_1") || json.isNull("team_2")) {
				return null;
			}
			
			MTeam team1 = gson.fromJson(json.getJSONObject("team_1").toString(), MTeam.class);
			team1.setTeam(gson.fromJson(json.getJSONObject("team_1").getJSONObject("team").toString(), MTeamItem.class));
			if (!json.getJSONObject("team_1").isNull("coach")) {
				team1.setCoach(gson.fromJson(json.getJSONObject("team_1").getJSONObject("coach").toString(), MTeamItem.class));
			}
			
			MTeam team2 = gson.fromJson(json.getJSONObject("team_2").toString(), MTeam.class);
			team2.setTeam(gson.fromJson(json.getJSONObject("team_2").getJSONObject("team").toString(), MTeamItem.class));
			if (!json.getJSONObject("team_2").isNull("coach")) {
				team1.setCoach(gson.fromJson(json.getJSONObject("team_2").getJSONObject("coach").toString(), MTeamItem.class));
			}
			
			mMatch.setTeam1(team1);
			mMatch.setTeam2(team2);
		} catch (Exception e) {
			e.printStackTrace();
			Log.e("MMatch", "error parse team");
		}
		return mMatch;
	}

	public class MTeam extends MBase {
		
		@SerializedName("id")
		private String id;

		@SerializedName("goal")
		private int goal;

		@SerializedName("pen")
		private int pen;
		
		private MTeamItem team;
		private MTeamItem coach;
		public String getId() {
			return id;
		}
		public void setId(String id) {
			this.id = id;
		}
		public int getGoal() {
			return goal;
		}
		public void setGoal(int goal) {
			this.goal = goal;
		}
		public int getPen() {
			return pen;
		}
		public void setPen(int pen) {
			this.pen = pen;
		}
		public MTeamItem getTeam() {
			return team;
		}
		public void setTeam(MTeamItem team) {
			this.team = team;
		}
		public MTeamItem getCoach() {
			return coach;
		}
		public void setCoach(MTeamItem coach) {
			this.coach = coach;
		}
		
	}
	
	public class MTeamItem extends MBase {
		
		@SerializedName("name")
		private String name;
		
		@SerializedName("thumb")
		private String thumb;

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public String getThumb() {
			return thumb;
		}

		public void setThumb(String thumb) {
			this.thumb = thumb;
		}
		
	}
}
