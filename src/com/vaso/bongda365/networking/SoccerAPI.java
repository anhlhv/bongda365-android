package com.vaso.bongda365.networking;

import java.util.HashMap;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import android.app.ProgressDialog;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;

import com.androidquery.AQuery;
import com.androidquery.callback.AjaxCallback;
import com.vaso.bongda365.base.Constant;

public class SoccerAPI {
	/** Log */
	public static final String TAG = "ClipPlusAPI";

	public static void log(String message) {
		Log.e(TAG, message);
	}

	/** Config domain name */
	public static final boolean IS_TEST_MODE = false;
	public static final String DOMAIN_NAME = "http://bongda365.tv/api";
	public static final String TEST_DOMAIN_NAME = "http://clipbox.ilu.vn/api";

	/** Singleton */
	private AQuery aq;
	private static SoccerAPI instance;
	private Context context;

	/** Constructor */
	public static SoccerAPI getInstance() {

		if (instance == null) {
			instance = new SoccerAPI();
		}

		return instance;
	}

	public static SoccerAPI getInstance(Context context) {

		if (instance == null) {
			instance = new SoccerAPI();
		}

		if (instance.aq == null) {
			instance.aq = new AQuery(context);
		}

		instance.context = context;

		return instance;
	}

	/** Base request methods */
	public String getDomainName() {
		return IS_TEST_MODE ? TEST_DOMAIN_NAME : DOMAIN_NAME;
	}

	/**
	 * Generate URL only
	 * 
	 * @param path
	 * @return
	 */
	public String url(String path) {

		return getDomainName() + "/" + path + "?";
	}

	/**
	 * Generate URL from path & params
	 * 
	 * @param path
	 * @param params
	 * @return
	 */
	public String url(String path, Map<String, String> params) {
		String url = getDomainName() + "/" + path + "?";

		for (String key : params.keySet()) {
			url += key + "=" + params.get(key) + "&";
		}

		log(url);
		return url;
	}

	/**
	 * Base GET method
	 * 
	 * @param path
	 * @param params
	 * @param classType
	 * @param callback
	 */
	public <K> void get(String path, Map<String, String> params,
			Class<K> classType, AjaxCallback<K> callback) {
		String url = url(path, params);
		aq.ajax(url, classType, callback);
	}

	/**
	 * Base POST method
	 * 
	 * @param path
	 * @param params
	 * @param classType
	 * @param callback
	 */
	public <K> void post(String path, Map<String, Object> params,
			Class<K> classType, AjaxCallback<K> callback) {
		String url = url(path);
		aq.ajax(url, params, classType, callback);
	}

	/**
	 * Base POST method with Progress Dialog
	 * 
	 * @param dialog
	 * @param path
	 * @param params
	 * @param classType
	 * @param callback
	 */
	public <K> void postWithProgress(ProgressDialog dialog, String path,
			Map<String, Object> params, Class<K> classType,
			AjaxCallback<K> callback) {
		String url = url(path);
		aq.progress(dialog).ajax(url, params, classType, callback);
	}

	/******************************************************************************************************
	 ******************************************************************************************************/

	/**
	 * get article News
	 * 
	 * @param callback
	 */
	public void getNewsData(String menuId, int page, AjaxCallback<JSONArray> callback) {
		Map<String, String> params = new HashMap<String, String>();
		if (!TextUtils.isEmpty(menuId)) {
			params.put(Constant.PARAM_SORT, menuId);
		}
		params.put(Constant.PARAM_PAGE, String.valueOf(page));

		get(Constant.PATH_HOME_NEWS, params, JSONArray.class, callback);
	}

	/**
	 * get clips data
	 * 
	 * @param callback
	 */
	public void getClipsData(String menuId, int page, AjaxCallback<JSONArray> callback) {
		Map<String, String> params = new HashMap<String, String>();
		if (!TextUtils.isEmpty(menuId)) {
			params.put(Constant.PARAM_SORT, menuId);
		}
		params.put(Constant.PARAM_PAGE, String.valueOf(page));
		
		get(Constant.PATH_HOME_CLIPS, params, JSONArray.class, callback);
	}

	/**
	 * get left menu
	 * 
	 * @param callback
	 */
	public void getLeftMenuData(AjaxCallback<JSONObject> callback) {
		Map<String, String> params = new HashMap<String, String>();

		get(Constant.PATH_HOME_MENU, params, JSONObject.class, callback);
	}
	
	/**
	 * get matches data
	 * 
	 * @param callback
	 */
	public void getMatchesData(String menuId, int page, AjaxCallback<JSONArray> callback) {
		Map<String, String> params = new HashMap<String, String>();
		params.put(Constant.PARAM_PAGE, String.valueOf(page));
		if (!TextUtils.isEmpty(menuId)) {
			params.put(Constant.PARAM_SORT, menuId);
		}
		
		get(Constant.PATH_HOME_MATCH, params, JSONArray.class, callback);
	}
	
	/**
	 * get related video by videoId
	 * 
	 * @param videoId
	 * @param callback
	 */
	public void getRelatedClips(String videoId, int page, AjaxCallback<JSONArray> callback) {
		Map<String, String> params = new HashMap<String, String>();
		
		params.put(Constant.PARAM_PAGE, String.valueOf(page));
		
		get(Constant.PATH_RELATED_CLIPS + "/" + videoId, params, JSONArray.class, callback);
	}
	
}
