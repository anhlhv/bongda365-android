package com.vaso.bongda365.fragment;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebSettings.LayoutAlgorithm;
import android.webkit.WebView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.model.MNews;

@SuppressLint("ValidFragment")
public class NewsContentFragment extends BaseFragment {
	
	public static final String TAG = NewsContentFragment.class.getSimpleName();
	
	// UI components
	ImageView ivThumb;
	TextView tvTitle;
	WebView wvContent;
	LinearLayout llAds;
	
	// model data
	MNews mNews;
	
	public NewsContentFragment(MNews mNews){
		this.mNews = mNews;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news_content, container, false);
		
		findViews(view);
		
		return view;
	}

	private void findViews(View view) {
		// connect to UI components
		llAds = (LinearLayout) view.findViewById(R.id.layout_ads);
		ivThumb = (ImageView) view.findViewById(R.id.iv_thumb);
		tvTitle = (TextView) view.findViewById(R.id.tv_title);
		wvContent = (WebView) view.findViewById(R.id.wv_content);
		
		// init ads
		if(adView.getParent() != null){
			((ViewGroup) adView.getParent()).removeView(adView);
	     }
		llAds.addView(adView);
		adView.loadAd(adRequest);
		hideLayoutWhenFailedToLoadAds(llAds);
		
		// setting webview
		wvContent.getSettings().setLayoutAlgorithm(LayoutAlgorithm.SINGLE_COLUMN);
		wvContent.setHorizontalScrollBarEnabled(false);
		wvContent.setOnTouchListener(new View.OnTouchListener() {
			float m_downX;
			// disable horizontal scroll
            @SuppressLint("ClickableViewAccessibility")
			public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                case MotionEvent.ACTION_DOWN: {
                    // save the x
                    m_downX = event.getX();
                }
                    break;

                case MotionEvent.ACTION_MOVE:
                case MotionEvent.ACTION_CANCEL:
                case MotionEvent.ACTION_UP: {
                    // set x so that it doesn't move
                    event.setLocation(m_downX, event.getY());
                }
                    break;

                }

                return false;
            }
        });
//		wvContent.setInitialScale(1);
//		wvContent.getSettings().setLoadWithOverviewMode(true);
//		wvContent.getSettings().setUseWideViewPort(true);
		
		// fill data
		imageLoader.displayImage(mNews.getLargeImageUrl(), ivThumb);
		tvTitle.setText(mNews.getTitle());
		wvContent.loadDataWithBaseURL(null, mNews.getContent(), "text/html", "UTF-8", null);
	}
}
