package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.base.DataParsingController;
import com.vaso.bongda365.base.Utils;
import com.vaso.bongda365.model.MMenu;

public class NewsDetailsFragment extends BaseFragment {

	public static final String TAG = NewsDetailsFragment.class.getSimpleName();
	
	// UI components
	ViewPager mPager;
	PagerSlidingTabStrip tabs;
	LinearLayout llAds;
	
	// data
	List<MMenu> tabMenus;
	PagerAdapter adapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// init data
		tabMenus = new ArrayList<MMenu>();
		
		View view = inflater.inflate(R.layout.fragment_home, container, false);
		findViews(view);
		return view;
	}

	private void findViews(View view) {
		mPager = (ViewPager) view.findViewById(R.id.home_pager);
		tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
		llAds = (LinearLayout) view.findViewById(R.id.layout_ads);
		
		// init data
		getTabMenus();
		adapter = new PagerAdapter(getChildFragmentManager(), tabMenus);
		mPager.setAdapter(adapter);
		mPager.setOffscreenPageLimit(tabMenus.size());
		tabs.setViewPager(mPager);
		try {
			mPager.setCurrentItem(
					new JSONObject(Utils.getValueForKey(mContext, Constant.KEY_SAVED_ACTIVED_TAB)).getInt("news"));
		} catch (JSONException e) {
			e.printStackTrace();
		}
		
		// init ads
		if(adView.getParent() != null){
			((ViewGroup) adView.getParent()).removeView(adView);
	     }
		llAds.addView(adView);
		adView.loadAd(adRequest);
		hideLayoutWhenFailedToLoadAds(llAds);
	}
	
	private void getTabMenus(){
		try {
			JSONArray jsonMenus = new JSONArray(Utils.getValueForKey(mContext, Constant.KEY_SAVED_NEWS_TAB));
			List<MMenu> tmpMenus = DataParsingController.parseTabMenus(jsonMenus);
			for (MMenu mMenu : tmpMenus) {
				tabMenus.add(mMenu);
			}
		} catch (JSONException e) {
			e.printStackTrace();
		}
	}
	
	public class PagerAdapter extends FragmentPagerAdapter{
		
		private List<MMenu> menus;
		FragmentManager fm;

		public PagerAdapter(FragmentManager fm, List<MMenu> menus) {
			super(fm);
			this.menus = menus;
			this.fm = fm;
		}

		@Override
		public Fragment getItem(int position) {
			return findFragmentByPosition(position) == null ? 
					new NewsFragment(menus.get(position).getMenuId()) : findFragmentByPosition(position);
		}

		@Override
		public int getCount() {
			return menus.size();
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return menus.get(position).getTitle();
		}
		
//		private String makeFragmentName(int position)
//		{
//		     return "android:switcher:" + mPager.getId() + ":" + position;
//		}
		
	}
	
	public Fragment findFragmentByPosition(int position) {
	    return getChildFragmentManager().findFragmentByTag(
	            "android:switcher:" + mPager.getId() + ":"
	                    + position);
	}
	
}
