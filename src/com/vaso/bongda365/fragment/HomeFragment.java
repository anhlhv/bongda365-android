package com.vaso.bongda365.fragment;

import java.lang.reflect.Field;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.astuetz.PagerSlidingTabStrip;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;

public class HomeFragment extends BaseFragment {
	
	public static final String TAG = HomeFragment.class.getSimpleName();
	
	// UI components
	ViewPager mPager;
	PagerSlidingTabStrip tabs;
	LinearLayout llAds;
	
	// data
	PagerAdapter adapter;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		
		View view = inflater.inflate(R.layout.fragment_home, container, false);
		
		findViews(view);
		
		return view;
	}

	private void findViews(View view) {
		mPager = (ViewPager) view.findViewById(R.id.home_pager);
		tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
		llAds = (LinearLayout) view.findViewById(R.id.layout_ads);
		
		// init data
		adapter = new PagerAdapter(getChildFragmentManager());
		mPager.setAdapter(adapter);
		mPager.setOffscreenPageLimit(3);
		tabs.setViewPager(mPager);
		
		// init ads
		if(adView.getParent() != null){
			((ViewGroup) adView.getParent()).removeView(adView);
	     }
		llAds.addView(adView);
		adView.loadAd(adRequest);
		hideLayoutWhenFailedToLoadAds(llAds);
	}

	public class PagerAdapter extends FragmentPagerAdapter{

		public PagerAdapter(FragmentManager fm) {
			super(fm);
			// TODO Auto-generated constructor stub
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				
				return new SchedulerFragment("");
				
			case 1:
				
				return new NewsFragment("");
				
			case 2:
				
				return new ClipsFragment("");

			default:
				return null;
			}
		}

		@Override
		public int getCount() {
			// TODO Auto-generated method stub
			return 3;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			switch (position) {
			case 0:
				
				return getString(R.string.tab_scheduler);
				
			case 1:
				
				return getString(R.string.tab_news);
				
			case 2:
				
				return getString(R.string.tab_clips);

			default:
				return null;
			}
		}
		
	}
	
	/**
	 * fix exception "no activity"
	 */
	@Override
	public void onDetach() {
	    super.onDetach();

	    try {
	        Field childFragmentManager = Fragment.class.getDeclaredField("mChildFragmentManager");
	        childFragmentManager.setAccessible(true);
	        childFragmentManager.set(this, null);

	    } catch (NoSuchFieldException e) {
	        throw new RuntimeException(e);
	    } catch (IllegalAccessException e) {
	        throw new RuntimeException(e);
	    }
	}
}
