package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.actionbarsherlock.view.MenuItem;
import com.astuetz.PagerSlidingTabStrip;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.model.MMenu;

@SuppressLint("ValidFragment")
public class SchedulerContentFragment extends BaseFragment {
	
	public static final String TAG = SchedulerContentFragment.class.getSimpleName();
	
	// UI components
	ImageView ivFlagTeam1, ivFlagTeam2;
	TextView tvNameTeam1, tvNameTeam2;
	TextView tvScore, tvTime;
	ViewPager mPager;
	PagerSlidingTabStrip tabs;
	
	// data
	List<MMenu> tabMenus;
	PagerAdapter adapter;
	String matchId;
	
	public SchedulerContentFragment(String matchId) {
		this.matchId = matchId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// init data
		tabMenus = new ArrayList<MMenu>();
		
		View view = inflater.inflate(R.layout.fragment_scheduler_content, container, false);
		findViews(view);
		
		// config action bar
//		mActionBar.setHomeButtonEnabled(true);
//		mActionBar.setDisplayHomeAsUpEnabled(true);
		
		return view;
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == android.R.id.home) {
			getActivity().getSupportFragmentManager().popBackStack();
		}
		return super.onOptionsItemSelected(item);
	}

	private void findViews(View view) {
		// connect to UI components
		ivFlagTeam1 = (ImageView) view.findViewById(R.id.iv_flag_team_1);
		ivFlagTeam2 = (ImageView) view.findViewById(R.id.iv_flag_team_2);
		tvNameTeam1 = (TextView) view.findViewById(R.id.tv_name_team_1);
		tvNameTeam2 = (TextView) view.findViewById(R.id.tv_name_team_2);
		tvScore = (TextView) view.findViewById(R.id.tv_score);
		tvTime = (TextView) view.findViewById(R.id.tv_time);
		mPager = (ViewPager) view.findViewById(R.id.content_pager);
		tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
		
		// init fixed tabs
		MMenu mInfo = new MMenu(getString(R.string.tab_match_info), "match_info");
		MMenu mTable = new MMenu(getString(R.string.tab_table), "table");
		MMenu mStats = new MMenu(getString(R.string.tab_stats), "stats");
		tabMenus.add(mInfo);
		tabMenus.add(mTable);
		tabMenus.add(mStats);
		
		// init data
		adapter = new PagerAdapter(getChildFragmentManager());
		mPager.setAdapter(adapter);
		mPager.setOffscreenPageLimit(tabMenus.size());
		tabs.setViewPager(mPager);
	}
	
	public class PagerAdapter extends FragmentPagerAdapter{

		public PagerAdapter(FragmentManager fm) {
			super(fm);
		}

		@Override
		public Fragment getItem(int position) {
			switch (position) {
			case 0:
				
				return new NewsFragment("");
				
			case 1:
				
				return new NewsFragment("");
				
			case 2:
				
				return new NewsFragment("");

			default:
				return null;
			}
		}

		@Override
		public int getCount() {
			return 3;
		}
		
		@Override
		public CharSequence getPageTitle(int position) {
			return tabMenus.get(position).getTitle();
		}
		
	}

}
