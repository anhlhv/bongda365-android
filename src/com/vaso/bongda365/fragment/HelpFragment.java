package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONException;
import org.json.JSONObject;

import android.content.Intent;
import android.content.pm.LabeledIntent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.base.Utils;
import com.vaso.bongda365.widget.WebViewPopup;

public class HelpFragment extends BaseFragment implements OnClickListener{
	
	public static final String TAG = HelpFragment.class.getSimpleName();
	
	// UI components
	Button btnRemoveAds, btnMoreApps, btnFreeApps, btnReport, btnRate;
	TextView tvTitle, tvDescription;
	
	// data
	String title, description, email, freeAppsUrl;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_help, container, false);
		
		// get data from shared preference
		if (!TextUtils.isEmpty(Utils.getValueForKey(mContext, Constant.KEY_SAVED_HELP))) {
			JSONObject json;
			try {
				json = new JSONObject(Utils.getValueForKey(mContext, Constant.KEY_SAVED_HELP));
				title = json.getString("title");
				description = json.getString("content");
				email = json.getString("email");
				freeAppsUrl = json.getString("free_app_url");
			} catch (JSONException e) {
				Log.e(TAG, "parse help data error");
				e.printStackTrace();
			}
			
		}
		
		findViews(view);
		
		return view;
	}

	private void findViews(View view) {
		// connect to UI components
		tvTitle = (TextView) view.findViewById(R.id.tv_title);
		tvDescription = (TextView) view.findViewById(R.id.tv_description);
		btnRemoveAds = (Button) view.findViewById(R.id.btn_remove_ads);
		btnMoreApps = (Button) view.findViewById(R.id.btn_more_apps);
		btnFreeApps = (Button) view.findViewById(R.id.btn_free_apps);
		btnReport = (Button) view.findViewById(R.id.btn_report);
		btnRate = (Button) view.findViewById(R.id.btn_rate);
		
		// fill data
		if (!TextUtils.isEmpty(title)) {
			tvTitle.setText(title);
		}
		if (!TextUtils.isEmpty(description)) {
			tvDescription.setText(description);
		}
		
		// setup action
		btnRemoveAds.setOnClickListener(this);
		btnMoreApps.setOnClickListener(this);
		btnFreeApps.setOnClickListener(this);
		btnReport.setOnClickListener(this);
		btnRate.setOnClickListener(this);
	}

	@Override
	public void onClick(View v) {
		if (v == btnRemoveAds) {
			openStore(Constant.APP_PACKAGE_NAME);
			
		} else if (v == btnMoreApps) {
			cb.showMoreApps();
			
		} else if (v == btnFreeApps) {
			View popup = LayoutInflater.from(mContext).inflate(R.layout.popup_webview, null, false);
			new WebViewPopup(mContext, freeAppsUrl, LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT, popup, this.getView());
			
		} else if (v == btnReport) {
			Intent emailIntent = new Intent(Intent.ACTION_SEND);
			emailIntent.putExtra(Intent.EXTRA_EMAIL, new String[] { email });
			emailIntent.putExtra(Intent.EXTRA_SUBJECT, "");
			emailIntent.putExtra(Intent.EXTRA_TEXT, "");
 
//			// need this to prompts email client only
			emailIntent.setType("text/plain");
			
			PackageManager pm = getActivity().getPackageManager();
		    Intent sendIntent = new Intent(Intent.ACTION_SEND);     
		    sendIntent.setType("text/plain");
			
			Intent openInChooser = Intent.createChooser(emailIntent, "Choose an Email client");

		    List<ResolveInfo> resInfo = pm.queryIntentActivities(sendIntent, 0);
		    List<LabeledIntent> intentList = new ArrayList<LabeledIntent>();        
		    for (int i = 0; i < resInfo.size(); i++) {
		        // Extract the label, append it, and repackage it in a LabeledIntent
		        ResolveInfo ri = resInfo.get(i);
		        String appName = ri.activityInfo.name;
		        if(appName.contains("mail")) {
					emailIntent.setPackage(ri.activityInfo.packageName);
					
					intentList.add(new LabeledIntent(emailIntent, ri.activityInfo.packageName, ri.loadLabel(pm), ri.icon));
		        }
		    }

		    // convert intentList to array
		    LabeledIntent[] extraIntents = intentList.toArray( new LabeledIntent[ intentList.size() ]);

		    openInChooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, extraIntents);
 
			startActivity(openInChooser);
			
		} else if (v == btnRate) {
			openStore(Constant.APP_PACKAGE_NAME);
		}
	}

	private void openStore(String packageName) {
		final String appPackageName = getActivity().getPackageName(); // getPackageName() from Context or Activity object
		try {
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
		} catch (android.content.ActivityNotFoundException anfe) {
		    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://play.google.com/store/apps/details?id=" + appPackageName)));
		}
	}
}
