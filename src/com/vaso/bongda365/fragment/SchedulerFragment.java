package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.annotation.SuppressLint;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.paging.listview.PagingExpandableListView;
import com.paging.listview.PagingExpandableListView.Pagingable;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.model.MMatch;
import com.vaso.bongda365.networking.SoccerAPI;

@SuppressLint("ValidFragment")
public class SchedulerFragment extends BaseFragment {
	
	public static final String TAG = SchedulerFragment.class.getSimpleName();
	
	// UI components
	private PullToRefreshLayout mPullToRefreshLayout;
	PagingExpandableListView lvMatches;
	
	// data
	List<MGroupMatch> group;
	GroupMatchAdapter adapter;
	String menuId;
	
	// paging
	int page;
	
	public SchedulerFragment(String menuId) {
		this.menuId = menuId;
	}

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_scheduler, container, false);
		
		if (group == null) {
			group = new ArrayList<MGroupMatch>();
		}
		
		findViews(view);

		if (group.isEmpty()) {
			getMatchesData();
		}
		
		return view;
	}
	
	private void findViews(View view) {
		// connect to UI components
		mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.ptr_layout);
		lvMatches = (PagingExpandableListView) view.findViewById(R.id.lv_matches);
		
		// setup pull to refresh
		ActionBarPullToRefresh.from(getActivity())
			.allChildrenArePullable()
			.listener(new OnRefreshListener() {
		
				@Override
				public void onRefreshStarted(View view) {
					page = 0;
					group.clear();
					getMatchesData();
				}
			}).setup(mPullToRefreshLayout);
		
		// setup data
		adapter = new GroupMatchAdapter(group);
		lvMatches.setAdapter(adapter);
		lvMatches.setHasMoreItems(true);
		lvMatches.setPagingableListener(new Pagingable() {
			
			@Override
			public void onLoadMoreItems() {
				getMatchesData();
			}
		});
		
		// setup listener
//		lvMatches.setOnChildClickListener(new OnChildClickListener() {
//			
//			@Override
//			public boolean onChildClick(ExpandableListView parent, View v,
//					int groupPosition, int childPosition, long id) {
//				// TODO show detail when click to match on list
//				MMatch mMatch = group.get(groupPosition).getMatches().get(childPosition);
//				Log.e(TAG, "match id: " + mMatch.getId());
//				switchContent(new SchedulerContentFragment(mMatch.getId()), SchedulerContentFragment.TAG, true, false);
//				return false;
//			}
//		});
	}

	@SuppressLint("NewApi")
	private void getMatchesData() {
		lvMatches.setEnabled(false);
		page++;
		SoccerAPI.getInstance(mContext).getMatchesData(menuId, page, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray jsonArr, AjaxStatus status) {
				try {
					if (jsonArr != null) {
						if (jsonArr.length() == 0) {
							lvMatches.onFinishLoading(false, null);
							
							if (mPullToRefreshLayout.isRefreshing()) {
								mPullToRefreshLayout.setRefreshComplete();
							}
							
							lvMatches.setEnabled(true);
							return;
							
						} else {
							for (int i = 0; i < jsonArr.length(); i++) {
								JSONObject json = jsonArr.getJSONObject(i);
								Iterator<?> keys = json.keys();
								if (keys.hasNext()) {
									MGroupMatch gMatches = new MGroupMatch();
									List<MMatch> matches = new ArrayList<MMatch>();
									String key = (String) keys.next();
									gMatches.setDate(key);
									
					                JSONArray jsonArrMatch = json.getJSONArray(key);
					                for (int j = 0; j < jsonArrMatch.length(); j++) {
										MMatch mMatch = MMatch.createMMatch(jsonArrMatch.getJSONObject(j));
										if (mMatch != null) {
											matches.add(mMatch);
										}
									}
					                
					                if (!matches.isEmpty()) {
						                gMatches.setMatches(matches);
						                group.add(gMatches);
									}
								}
							}
							lvMatches.onFinishLoading(true, group);
						}
						
						if (mPullToRefreshLayout.isRefreshing()) {
							mPullToRefreshLayout.setRefreshComplete();
						}
						
						adapter.notifyDataSetChanged();
						
						// auto expand list view
						try {
							if (!group.isEmpty()) {
								for (int i = 0; i < group.size(); i++) {
									lvMatches.expandGroup(i, true);
								}
							}
						} catch (Exception e) {
							e.printStackTrace();
						}
						
						lvMatches.setEnabled(true);
					}
				} catch (Exception e) {
					e.printStackTrace();
					Log.e(TAG, "parse matches error");
				}
			}
		});
	}
	
	public class GroupMatchAdapter extends BaseExpandableListAdapter {
		
		List<MGroupMatch> group;
		HeaderHolder headerHolder;
		ChildHolder childHolder;
		
		public GroupMatchAdapter(List<MGroupMatch> group) {
			this.group = group;
		}

		@Override
		public Object getChild(int groupPosition, int childPosition) {
			return group.get(groupPosition).getMatches().get(childPosition);
		}
	
		@Override
		public long getChildId(int groupPosition, int childPosition) {
			return childPosition;
		}
	
		@Override
		public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView,
				ViewGroup parent) {
			MMatch match = (MMatch) getChild(groupPosition, childPosition);
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_scheduler, parent, false);
				childHolder = new ChildHolder();
				childHolder.tvMatchLeague = (TextView) convertView.findViewById(R.id.tv_match_league);
				childHolder.tvNameTeam1 = (TextView) convertView.findViewById(R.id.tv_name_team_1);
				childHolder.tvNameTeam2 = (TextView) convertView.findViewById(R.id.tv_name_team_2);
				childHolder.tvTimeAndScrore = (TextView) convertView.findViewById(R.id.tv_time_and_score);
				childHolder.ivFlagTeam1 = (ImageView) convertView.findViewById(R.id.iv_flag_team_1);
				childHolder.ivFlagTeam2 = (ImageView) convertView.findViewById(R.id.iv_flag_team_2);
				
				convertView.setTag(childHolder);
			} else {
				childHolder = (ChildHolder) convertView.getTag();
			}
			
			// fill data
			childHolder.tvMatchLeague.setText(match.getLeague());
			childHolder.tvNameTeam1.setText(match.getTeam1().getTeam().getName());
			childHolder.tvNameTeam2.setText(match.getTeam2().getTeam().getName());
			if (match.isStarted()) {
				childHolder.tvTimeAndScrore.setText(match.getTeam1().getGoal() + " : " + match.getTeam2().getGoal());
			} else {
				childHolder.tvTimeAndScrore.setText(match.getTime());
			}
			imageLoader.displayImage(match.getTeam1().getTeam().getThumb(), childHolder.ivFlagTeam1);
			imageLoader.displayImage(match.getTeam2().getTeam().getThumb(), childHolder.ivFlagTeam2);
			
			return convertView;
		}
	
		@Override
		public int getChildrenCount(int groupPosition) {
			return group.get(groupPosition).getMatches().size();
		}
	
		@Override
		public Object getGroup(int groupPosition) {
			return group.get(groupPosition);
		}
	
		@Override
		public int getGroupCount() {
			return group.size();
		}
	
		@Override
		public long getGroupId(int groupPosition) {
			return groupPosition;
		}
	
		@Override
		public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
			MGroupMatch mGroup = group.get(groupPosition);
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_scheduler_header, parent, false);
				headerHolder = new HeaderHolder();
				headerHolder.tvTitleGroup = (TextView) convertView.findViewById(R.id.tv_title_group);
				
				convertView.setTag(headerHolder);
			} else {
				headerHolder = (HeaderHolder) convertView.getTag();
			}
			
			// fill data
			headerHolder.tvTitleGroup.setText(mGroup.getDate());
			
			return convertView;
		}
	
		@Override
		public boolean hasStableIds() {
			// TODO Auto-generated method stub
			return false;
		}
	
		@Override
		public boolean isChildSelectable(int groupPosition, int childPosition) {
			// TODO Auto-generated method stub
			return true;
		}
		
	}
	
	private static class ChildHolder {
		public TextView tvMatchLeague;
		public TextView tvNameTeam1, tvNameTeam2;
		public TextView tvTimeAndScrore;
		public ImageView ivFlagTeam1, ivFlagTeam2;
	}
	
	private static class HeaderHolder {
		public TextView tvTitleGroup;
	}
	
	private class MGroupMatch {
		
		private String date;
		private List<MMatch> matches;
		
		public String getDate() {
			return date;
		}
		public void setDate(String date) {
			this.date = date;
		}
		public List<MMatch> getMatches() {
			return matches;
		}
		public void setMatches(List<MMatch> matches) {
			this.matches = matches;
		}
	}
}
