package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.paging.listview.PagingListView;
import com.paging.listview.PagingListView.Pagingable;
import com.vaso.bongda365.activity.ClipsContentActivity;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.base.Constant;
import com.vaso.bongda365.base.DataParsingController;
import com.vaso.bongda365.model.MClips;
import com.vaso.bongda365.networking.SoccerAPI;

@SuppressLint("ValidFragment")
public class ClipsFragment extends BaseFragment {

	public static final String TAG = ClipsFragment.class.getSimpleName();
	
	// UI components
	private PullToRefreshLayout mPullToRefreshLayout;
	private PagingListView lvClips;
	
	// data
	List<MClips> clips;
	ClipsAdapter adapter;
	String menuId;
	
	// paging
	public int page;
	
	public ClipsFragment(String menuId) {
		this.menuId = menuId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_clips, container, false);
		
		if (clips == null) {
			clips = new ArrayList<MClips>();
		}
		
		findViews(view);
		
		if (clips.isEmpty()) {
			getClipsData();
		}
		
		return view;
	}

	private void findViews(View view) {
		mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.ptr_layout);
		lvClips = (PagingListView) view.findViewById(R.id.lv_clips);
		
		// setup pull to refresh
		ActionBarPullToRefresh.from(getActivity())
			.theseChildrenArePullable(lvClips)
			.listener(new OnRefreshListener() {
		
				@Override
				public void onRefreshStarted(View view) {
					page = 0;
					clips.clear();
					getClipsData();
				}
			}).setup(mPullToRefreshLayout);
		
		// setup data
		adapter = new ClipsAdapter(mContext, R.layout.row_clips2, clips);
		lvClips.setAdapter(adapter);
		lvClips.setHasMoreItems(true);
		lvClips.setPagingableListener(new Pagingable() {
			
			@Override
			public void onLoadMoreItems() {
				getClipsData();
			}
		});
		
		lvClips.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position, long id) {
				String youtubeId = clips.get(position).getYoutubeId();
				String clipId= clips.get(position).getId();
				String clipTitle = clips.get(position).getTitle();

				Intent intent = new Intent(mContext, ClipsContentActivity.class);
				intent.putExtra(Constant.BUNDLE_CLIP_ID, clipId);
				intent.putExtra(Constant.BUNDLE_YOUTUBE_ID, youtubeId);
				intent.putExtra(Constant.BUNDLE_CLIP_TITLE, clipTitle);
				startActivity(intent);
			}
		});
	}
	
//	public class ClipsAdapter extends ArrayAdapter<MClips>{
//		
//		List<MClips> clips;
//		ClipsHolder holder;
//
//		public ClipsAdapter(Context context, int resource, List<MClips> data) {
//			super(context, resource, data);
//			this.clips = data;
//		}
//		
//		@Override
//		public View getView(int position, View convertView, ViewGroup parent) {
//			MClips mClips = clips.get(position);
//			
//			if (convertView == null) {
//				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_clips, parent, false);
//				holder = new ClipsHolder();
//				
//				holder.ivThumb = (ImageView) convertView.findViewById(R.id.iv_thumb);
//				holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
//				holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
//				holder.tvCommentCount = (TextView) convertView.findViewById(R.id.tv_comment_count);
//				holder.tvLikeCount = (TextView) convertView.findViewById(R.id.tv_like_count);
//				holder.tvViewCount = (TextView) convertView.findViewById(R.id.tv_view_count);
//				
//				convertView.setTag(holder);
//			} else {
//				holder = (ClipsHolder) convertView.getTag();
//			}
//			
//			// setup data
//			imageLoader.displayImage(mClips.getImageUrl(), holder.ivThumb);
//			holder.tvTitle.setText(mClips.getTitle());
//			holder.tvDate.setText(mClips.getPublishedAt());
//			holder.tvViewCount.setText(String.valueOf(mClips.getViewCount()));
//			holder.tvCommentCount.setText(String.valueOf(128));
//			holder.tvLikeCount.setText(String.valueOf(128));
//			
//			return convertView;
//		}
//	}
//	
//	public static class ClipsHolder{
//		ImageView ivThumb;
//		TextView tvTitle;
//		TextView tvDate;
//		TextView tvCommentCount, tvLikeCount, tvViewCount;
//	}
	
	public class ClipsAdapter extends ArrayAdapter<MClips>{
		
		List<MClips> clips;
		ClipsHolder holder;

		public ClipsAdapter(Context context, int resource, List<MClips> data) {
			super(context, resource, data);
			// TODO Auto-generated constructor stub
			this.clips = data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			MClips mClip = clips.get(position);
			
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_clips2, parent, false);
				holder = new ClipsHolder();
				holder.ivThumb = (ImageView) convertView.findViewById(R.id.iv_thumb);
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
				holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
				holder.tvViewCount = (TextView) convertView.findViewById(R.id.tv_view_count);
				
				convertView.setTag(holder);
			} else {
				holder = (ClipsHolder) convertView.getTag();
			}
			
			// setup data
			imageLoader.displayImage(mClip.getImageUrl(), holder.ivThumb);
			holder.tvTitle.setText(mClip.getTitle());
			holder.tvDate.setText(mClip.getPublishedAt());
			holder.tvViewCount.setText(String.valueOf(mClip.getViewCount()));
			
			return convertView;
		}
		
	}
	
	public static class ClipsHolder{
		ImageView ivThumb;
		TextView tvTitle;
		TextView tvDate;
		TextView tvViewCount;
	}
	
	private void getClipsData(){
		lvClips.setEnabled(false);
		page++;
		SoccerAPI.getInstance(mContext).getClipsData(menuId, page, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray jsonArr, AjaxStatus status) {
				List<MClips> lstTmp = DataParsingController.parseClips(jsonArr);
				if (lstTmp.isEmpty()) {
					lvClips.onFinishLoading(false, null);
				} else {
					clips.addAll(lstTmp);
					lvClips.onFinishLoading(true, clips);
				}
				
				if (mPullToRefreshLayout.isRefreshing()) {
					mPullToRefreshLayout.setRefreshComplete();
				}
				
				adapter.notifyDataSetChanged();
				lvClips.setEnabled(true);
			}
		});
	}
	
}
