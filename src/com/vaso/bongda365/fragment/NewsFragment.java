package com.vaso.bongda365.fragment;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;

import uk.co.senab.actionbarpulltorefresh.extras.actionbarsherlock.PullToRefreshLayout;
import uk.co.senab.actionbarpulltorefresh.library.ActionBarPullToRefresh;
import uk.co.senab.actionbarpulltorefresh.library.listeners.OnRefreshListener;
import android.annotation.SuppressLint;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.androidquery.callback.AjaxCallback;
import com.androidquery.callback.AjaxStatus;
import com.paging.listview.PagingListView;
import com.paging.listview.PagingListView.Pagingable;
import com.vaso.bongda365.activity.R;
import com.vaso.bongda365.base.BaseFragment;
import com.vaso.bongda365.base.DataParsingController;
import com.vaso.bongda365.model.MNews;
import com.vaso.bongda365.networking.SoccerAPI;

@SuppressLint("ValidFragment")
public class NewsFragment extends BaseFragment {
	
	public static final String TAG = NewsFragment.class.getSimpleName();
	
	// UI components
	private PullToRefreshLayout mPullToRefreshLayout;
	private PagingListView lvNews;
	
	// data
	List<MNews> news;
	NewsAdapter adapter;
	String menuId;
	
	// paging
	public int page;
	
	public NewsFragment(String menuId) {
		this.menuId = menuId;
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		View view = inflater.inflate(R.layout.fragment_news, container, false);
		
		if (news == null) {
			news = new ArrayList<MNews>();
		}
		
		findViews(view);
		
		if (news.isEmpty()) {
			getNewsData();
		}
		
		return view;
	}

	private void findViews(View view) {
		mPullToRefreshLayout = (PullToRefreshLayout) view.findViewById(R.id.ptr_layout);
		lvNews = (PagingListView) view.findViewById(R.id.lv_news);
		
		// setup pull to refresh
		ActionBarPullToRefresh.from(getActivity())
			.theseChildrenArePullable(lvNews)
			.listener(new OnRefreshListener() {
		
				@Override
				public void onRefreshStarted(View view) {
					page = 0;
					news.clear();
					getNewsData();
				}
			}).setup(mPullToRefreshLayout);
		
		// setup data listview
		adapter = new NewsAdapter(mContext, R.layout.row_news, news);
		lvNews.setAdapter(adapter);
		lvNews.setHasMoreItems(true);
		lvNews.setPagingableListener(new Pagingable() {
			
			@Override
			public void onLoadMoreItems() {
				getNewsData();
			}
		});
		
		lvNews.setOnItemClickListener(new OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> arg0, View view, int position,
					long id) {
				MNews mNews = news.get(position);
				switchContent(new NewsContentFragment(mNews), NewsContentFragment.TAG, true, false);
			}
		});
	}
	
	public class NewsAdapter extends ArrayAdapter<MNews>{
		
		List<MNews> news;
		NewsHolder holder;

		public NewsAdapter(Context context, int resource, List<MNews> data) {
			super(context, resource, data);
			// TODO Auto-generated constructor stub
			this.news = data;
		}
		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			MNews mNews = news.get(position);
			
			if (convertView == null) {
				convertView = LayoutInflater.from(mContext).inflate(R.layout.row_news, parent, false);
				holder = new NewsHolder();
				holder.ivThumb = (ImageView) convertView.findViewById(R.id.iv_thumb);
				holder.tvTitle = (TextView) convertView.findViewById(R.id.tv_title);
				holder.tvDate = (TextView) convertView.findViewById(R.id.tv_date);
				holder.tvViewCount = (TextView) convertView.findViewById(R.id.tv_view_count);
				
				convertView.setTag(holder);
			} else {
				holder = (NewsHolder) convertView.getTag();
			}
			
			// setup data
			imageLoader.displayImage(mNews.getImageUrl(), holder.ivThumb);
			holder.tvTitle.setText(mNews.getTitle());
			holder.tvDate.setText(mNews.getPublishedAt());
			holder.tvViewCount.setText(String.valueOf(mNews.getViewCount()));
			
			return convertView;
		}
		
	}
	
	public static class NewsHolder{
		ImageView ivThumb;
		TextView tvTitle;
		TextView tvDate;
		TextView tvViewCount;
	}
	
	private void getNewsData(){
		lvNews.setEnabled(false);
		page++;
		SoccerAPI.getInstance(mContext).getNewsData(menuId, page, new AjaxCallback<JSONArray>(){
			@Override
			public void callback(String url, JSONArray jsonArr, AjaxStatus status) {
				List<MNews> lstTmp = DataParsingController.parseNews(jsonArr);
				if (lstTmp.isEmpty()) {
					lvNews.onFinishLoading(false, null);
				} else {
					news.addAll(lstTmp);
					lvNews.onFinishLoading(true, news);
				}
				
				if (mPullToRefreshLayout.isRefreshing()) {
					mPullToRefreshLayout.setRefreshComplete();
				}
				
				adapter.notifyDataSetChanged();
				lvNews.setEnabled(true);
			}
		});
	}

}
