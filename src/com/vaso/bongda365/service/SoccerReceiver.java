package com.vaso.bongda365.service;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.vaso.bongda365.activity.MainActivity;
import com.vaso.bongda365.activity.R;

public class SoccerReceiver extends BroadcastReceiver {

	public static final String TAG = SoccerReceiver.class.getSimpleName();
	
	public static final int NOTIFICATION_ID = 1;
    NotificationCompat.Builder builder;

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
			JSONObject json = new JSONObject(intent.getExtras().getString(
					"com.parse.Data"));
			
//			String action = intent.getAction();
//			String channel = intent.getExtras().getString("com.parse.Channel");
//
//			Log.e(TAG, "got action " + action + " on channel " + channel
//					+ " with:");
//			Iterator<?> itr = json.keys();
//			while (itr.hasNext()) {
//				String key = (String) itr.next();
//				Log.e(TAG, "..." + key + " => " + json.getString(key));
//			}
			
			showNotification(context, json.getString("message"));
		} catch (JSONException e) {
			Log.e(TAG, "JSONException: " + e.getMessage());
		}
	}
	
	private void showNotification(Context context, String message) {
		Intent intent = new Intent(context, MainActivity.class);
	    PendingIntent contentIntent = PendingIntent.getActivity(context, 0,
	            intent, 0);

	    NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context)
	        .setSmallIcon(R.drawable.ic_launcher)
	        .setContentTitle(context.getString(R.string.app_name))
	        .setStyle(new NotificationCompat.BigTextStyle()
	        .bigText(message))
	        .setContentText(message);
	    
	    mBuilder.setContentIntent(contentIntent);
	    mBuilder.setDefaults(Notification.DEFAULT_SOUND);
	    mBuilder.setAutoCancel(true);
	    NotificationManager mNotificationManager =
	        (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
	    mNotificationManager.notify(1, mBuilder.build());

	} 

}
